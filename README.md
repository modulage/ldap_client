# README #

This LDAP client will expose your LDAP directory as a JSON object.

### How do I get set up? (Windows) ###

* Step 1: NodeJS

    * Download and install the latest version of NodeJS at https://nodejs.org/

* Step 2: LDAP client

    * Download and extract our LDAP client to a folder of your choice

* Step 3: Config

    * Edit config.json to match your environment.

* Step 4: Run

    * Open a command prompt, navigate to the folder where your ldap client resides, and run 
        * "npm install"
        * "node server.js"
    * If everything works as intended, you should see "Server is listening"
    * You can now use a browser to test the LDAP http server by pointing your browser to http://ip_or_hostname_of_machine_running_ldap_client:8080 
    * If you're running a firewall, make sure it allows access to port 8080. 
    * If your browser shows a JSON object representing the users you want to expose, than everything works! 

* Step 5 (optional): Use NSSM to daemonize the node process as a Windows Service.

    * Download the latest release of NSSM at https://nssm.cc/download
    * Extract the appropriate architecture executable to a folder of your choice.
    * Now run the following command: nssm install ldap_client
    * This will show a window where your service parameters can be configured.
        * Select "node.exe" as Path. It can usually be found in c:\Program Files (x86)\nodejs\node.exe
        * Edit the Startup directory and point it to the folder where your LDAP client resides
        * Type the full path to server.js in the Arguments field.
        * Optionally, you can set a Display name for your service in the Details tab.
    * Now go to your Windows Services, start your newly created service, and test! Remember to stop any manually started instances of the LDAP client before starting the service.

### How do I get set up? (Linux) ###

* Step 1: NodeJS

    * Use your favorite package manager to install NodeJS

* Step 2: LDAP client

    * Download and extract our LDAP client to a directory of your choice

* Step 3: Config

    * Edit config.json to match your environment.

* Step 4: Run

    * run the following command:
        * "npm install"
        *  node /path/to/ldapclient/server.js
    * If everything works as intended, you should see "Server is listening"
    * You can now use a browser to test the LDAP http server by pointing your browser to http://ip_or_hostname_of_machine_running_ldap_client:8080
    * If you're running a firewall, make sure it allows access to port 8080.
    * If your browser shows a JSON object representing the users you want to expose, than everything works!

* Step 5 (optional): Use PM2 to daemonize the node process as a Windows Service.

    * Please refer to https://github.com/Unitech/pm2 for instructions on how to install and use PM2.

### Troubleshooting 

* Demo services are available for testing purposes. They include names such as "Tesla", "Newton", etc..

    * HTTP version: http://178.162.136.223:48800
    * HTTPS version: https://source.vizito.eu:48801


### Who do I talk to ###
* support@vizito.be
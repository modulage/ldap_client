var ldap = require('ldapjs');
var http = require("http");
var https = require('https');
var config = require('./config.json');
var fs = require('fs');
var scheduler = require('node-schedule');
var os = require("os");
var crypto = require('crypto');

var algorithm = 'aes-256-cbc';
var password = crypto.scryptSync(os.hostname(), 'salt', 32) ;

require('console-stamp')(console, 'yyyy:mm:dd-HH:MM:ss.l');

var cachedResults = [];
var ldapPassword = '';

if (config.ldap_bind_credentials.password) {
    ldapPassword = config.ldap_bind_credentials.password;
    delete config.ldap_bind_credentials.password;
    config.ldap_bind_credentials.iv = crypto.randomBytes(16);
    var key = crypto.createCipheriv(algorithm, password, config.ldap_bind_credentials.iv);
    var encrypted_str = key.update(ldapPassword, 'utf8', 'hex')
    encrypted_str += key.final('hex');
    config.ldap_bind_credentials.encryptedPassword = encrypted_str;
    let data = JSON.stringify(config, null, 2);
    fs.writeFileSync('config.json', data);
} else if (config.ldap_bind_credentials.encryptedPassword) {
    const iv = Buffer.from(config.ldap_bind_credentials.iv, 'hex');
    var key = crypto.createDecipheriv(algorithm, password, iv);
    var decrypted_str = key.update(config.ldap_bind_credentials.encryptedPassword, 'hex', 'utf8')
    decrypted_str += key.final('utf8');
    ldapPassword = decrypted_str;
}

function encrypt(buffer, algorithm, password) {
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = Buffer.concat([cipher.update(buffer), cipher.final()]);
    return crypted;
}

function decrypt(buffer, algorithm, password) {
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = Buffer.concat([decipher.update(buffer), decipher.final()]);
    return dec;
}

function runserver(request, response) {

    var ip = request.headers['x-forwarded-for'] ||
        request.connection.remoteAddress ||
        request.socket.remoteAddress ||
        request.connection.socket.remoteAddress;

    console.log('Request from ' + ip);

    if (request.method != "OPTIONS") {
        if (config.authentication.enabled && !isAuthenticated(request, response)) {
            response.statusCode = 401;
            response.setHeader('WWW-Authenticate', 'Basic realm="Secure Area"');
            response.writeHead(401, {"Content-Type": "text/html; charset=utf-8", "Access-Control-Allow-Origin" : "*", "Access-Control-Allow-Headers" :  "X-Requested-With, Content-Type, Authorization, Language"});
            response.end('<html><body>Need some credentials</body></html>');
            return;
        }
    }

    response.writeHead(200, {"Content-Type": "application/json", "Access-Control-Allow-Origin" : "*", "Access-Control-Allow-Headers" :  "X-Requested-With, Content-Type, Authorization, Language"});
    response.write(cachedResults);
    response.end();

};

function isAuthenticated(request, response) {
    var auth = request.headers['authorization'];

    if(!auth) {
        return false;
    } else if(auth) {
        var tmp = auth.split(' ');
        var buf = new Buffer(tmp[1], 'base64');
        var plain_auth = buf.toString();
        var creds = plain_auth.split(/:(.+)/);
        var username = creds[0];
        var password = creds[1];
        if((username == config.authentication.username) && (password == config.authentication.password)) {
            return true;
        }
        else {
            return false;
        }
    }
}

var hourlyRule = new scheduler.RecurrenceRule();
hourlyRule.minute = 0;

scheduler.scheduleJob(hourlyRule, fetchData);
var i = 0;
var cnList = [];
var counter = 0;

function fetchData() {
    try {
        i = 0;
        cnList = [];
        counter = 0;
        var client = ldap.createClient(JSON.parse('{"url" : "' + config.ldap_url + '", "tlsOptions": {"rejectUnauthorized": false}, "reconnect": true}'));
    } catch(e) {
        console.error("LDAP client creation failed: " + e);
        return;
    }

    client.on('error', function(err) {
        console.warn('LDAP connection failed, but fear not, it will reconnect.', err);
        return;
    });

    client.bind(config.ldap_bind_credentials.user_cn, ldapPassword, function(err) {
        if (err) {
            console.error("LDAP bind failed: " + err);
            return;
        }

        var opts = {
            filter: config.ldap_filter,
            scope: config.search_scope || 'one',
            attributes: config.returned_attributes.replace(/ /g, '').split(','),
            paged: true,
            sizeLimit: 200
        };

        config.search_base.forEach(function (value) {
            i++
            setTimeout(function(){
                process_search_base(client, value, opts);
              }, i*500)
        });
    });
}

function process_search_base(client, value, opts) {
    console.log("Retrieving data from: " + value);
    client.search(value, opts, function (err, res) {
        i++;
        res.on('searchEntry', function(entry) {
            var result = JSON.parse(JSON.stringify(entry.object));
            if (result.mobile && config.mobile_prefix ) {
                if (config.mobile_prefix != "" && result.mobile.substring(0,1) == "0") {
                    result.mobile = config.mobile_prefix + result.mobile.substring(1);
                } else {
                    result.mobile = config.mobile_prefix + result.mobile;
                }
            }
            if ((!result.mobile || result.mobile === undefined || result.mobile == "") && result.otherMobile && result.otherMobile != "" ) {
                if (config.mobile_prefix != "" && result.otherMobile.substring(0,1) == "0") {
                    result.mobile = config.mobile_prefix + result.otherMobile.substring(1);
                } else {
                    result.mobile = config.mobile_prefix + result.otherMobile;
                }
                delete result.otherMobile;
            }
            if (result.mobile !== undefined && result.telephoneNumber){
                delete result.telephoneNumber;
            }
            if (result.cn != undefined) {
                cnList.push(result);
            }
        });
        res.on('searchReference', function(referral) {
            console.log('referral: ' + referral.uris.join());
        });
        res.on('error', function(err) {
            console.error('error: ' + err.message);
            client.unbind(function(err) {
                if (err)
                    console.error('error: ' + err.message);
            });
        });
        res.on('end', function(result) {
            counter++;
            if (counter == config.search_base.length){
                cachedResults = JSON.stringify(cnList);
                console.log('success');
            }
        });
    });
};

fetchData();

var server;

if (config.use_ssl_for_api) {
    var caArray = [];
    if (config.ssl.ca && config.ssl.ca.length > 0) {
        config.ssl.ca.forEach(function (caReference) {
            caArray.push(fs.readFileSync(caReference));
        });
    }

    server = https.createServer({
        ca: caArray,
        key: fs.readFileSync(config.ssl.key),
        cert: fs.readFileSync(config.ssl.cert),
        ciphers: [
            "ECDHE-RSA-AES256-SHA384",
            "DHE-RSA-AES256-SHA384",
            "ECDHE-RSA-AES256-SHA256",
            "DHE-RSA-AES256-SHA256",
            "ECDHE-RSA-AES128-SHA256",
            "DHE-RSA-AES128-SHA256",
            "HIGH",
            "!aNULL",
            "!eNULL",
            "!EXPORT",
            "!DES",
            "!RC4",
            "!MD5",
            "!PSK",
            "!SRP",
            "!CAMELLIA"
        ].join(':')
    }, function(request, response) {
        runserver(request, response);
    });
} else {
    server = http.createServer(function(request, response) {
        runserver(request, response);
    });
}

server.listen(config.api_port);

console.log((config.use_ssl_for_api ? "HTTPS" : "HTTP") + " server is listening on port " + config.api_port);
